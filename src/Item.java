import java.util.Comparator;

class Item implements Comparator<Item> {
    int id;
    double profit;
    double weight;

    Item(int id, double profit, double weight) {
        this.id = id;
        this.profit = profit;
        this.weight = weight;
    }

    @Override
    public int compare(Item o1, Item o2) {
        return (int) (o1.profit - o2.profit);
    }
}
