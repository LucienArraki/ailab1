import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class Indiv {
    public double weight = 0;
    public double profit = 0;
    public double timing;
    double rating = -1000000000;
    private Random random = new Random();
    private List<City> genotype = new ArrayList<>();
    private List<Item> indivItem = new ArrayList<>();

    Indiv() {
        genotype.addAll(DataFromFile.cityMap.values());
        for (int i = 0; i < random.nextInt(1000); i++) {
            mutate();
        }
        evaluate();
    }

    Indiv(Indiv indiv) {
        this.rating = indiv.rating;
        this.genotype = new ArrayList<>();
        for (City m : indiv.genotype) {
            if (m != null) {
                this.genotype.add(new City(m));
            }
        }
        fixGenotyp();
        evaluate();
    }

    Indiv(List<City> genotype) {
        this.genotype = new ArrayList<>();
        for (City m : genotype) {
            if (m != null) {
                this.genotype.add(new City(m));
            }
        }
        fixGenotyp();
        evaluate();
    }

    private void evaluate() {
        rating = 0;
        weight = 0;
        for (Item item : indivItem) {
            weight += item.weight;
        }

        double gy = secondProblem();
        double fx = firstProblem();
        profit = gy;
        timing = fx;

        rating = gy*10 - fx;
    }

    private double firstProblem() {
        double fx = 0;
        double d = 0;
        double vc = DataFromFile.maxSpeed - (weight * ((DataFromFile.maxSpeed - DataFromFile.minSpeed) / DataFromFile.capacityOfKnapsack));

        for (int i = 0; i < genotype.size() - 1; i++) {
            d = Math.sqrt(Math.pow((genotype.get(i).x - genotype.get(i + 1).x), 2) +
                    Math.pow((genotype.get(i).y - genotype.get(i + 1).y), 2));
            fx += d / vc;
        }
        d = Math.sqrt(Math.pow((genotype.get(1).x - genotype.get(genotype.size() - 1).x), 2) +
                Math.pow((genotype.get(1).y - genotype.get(genotype.size() - 1).y), 2));
        fx += d / vc;
        return fx;
    }

    private double secondProblem() {
        double gx = 0;
        for (City city : genotype) {
            List<Item> itemList = DataFromFile.itemMap.get(city.id);
            if (itemList != null) {
                for (Item przedmiot : itemList) {
                    if (weight + przedmiot.weight <= DataFromFile.capacityOfKnapsack) {
                        gx += przedmiot.profit;
                        weight += przedmiot.weight;
                    }
                }
            }
        }
        return gx;
    }

    void mutation(double pm) {
        if (random.nextDouble() < pm) {
            mutate();
        }
    }

    private void mutate() {
        int index1 = random.nextInt(genotype.size());
        int index2 = random.nextInt(genotype.size());
        while (index1 == index2) {
            index2 = random.nextInt(genotype.size());
        }
        City pom = genotype.get(index2);
        genotype.set(index2, genotype.get(index1));
        genotype.set(index1, pom);
    }

    private void fixGenotyp() {
        List<Integer> integerList = new ArrayList<>();

        for (int i = 1; i <= DataFromFile.cityMap.size(); i++) {
            integerList.add(DataFromFile.cityMap.get(i).id);
        }

        for (City city : genotype) {
            int genotypID = city.id;
            if (integerList.contains(genotypID)) {
                integerList.remove(integerList.indexOf(genotypID));
            }
        }

        if (!integerList.isEmpty()) {
            List<Integer> elementInGenotyp = new ArrayList<>();

            for (int i = 0; i < genotype.size(); i++) {
                int genotypID = genotype.get(i).id;
                if (elementInGenotyp.contains(genotypID)) {
                    genotype.set(i, DataFromFile.cityMap.get(integerList.get(0) + 1));
                    integerList.remove(0);
                    elementInGenotyp.add(genotype.get(i).id);
                } else {
                    elementInGenotyp.add(genotypID);
                }
            }
        }
    }

    List<City> getGenotype() {
        return genotype;
    }
}
