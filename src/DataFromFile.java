import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

class DataFromFile {
    static Map<Integer, City> cityMap = new TreeMap<>();
    static Map<Integer, List<Item>> itemMap = new TreeMap<>();
    static int capacityOfKnapsack = 0;
    static double minSpeed = 0;
    static double maxSpeed = 0;

    void getData() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("data/medium_0.ttp"));
            String line;

            while (!(line = reader.readLine()).contains("NODE_COORD_SECTION")) {
                if (line.contains("CAPACITY OF KNAPSACK")) {
                    capacityOfKnapsack = Integer.valueOf(line.substring(23));
                }
                if (line.contains("MIN SPEED")) {
                    minSpeed = Double.valueOf(line.substring(13));
                }
                if (line.contains("MAX SPEED")) {
                    maxSpeed = Double.valueOf(line.substring(12));
                }
            }

            int id = 0;
            while (!(line = reader.readLine()).contains("ITEMS SECTION")) {
                String[] splited = line.split("\t");
                cityMap.put(Integer.valueOf(splited[0]), new City(id++, Double.valueOf(splited[1]), Double.valueOf(splited[2])));
            }

            id = 0;
            while ((line = reader.readLine()) != null) {
                String[] splited = line.split("\t");
                Item item = new Item(
                        Integer.valueOf(splited[0]),
                        Integer.valueOf(splited[1]),
                        Integer.valueOf(splited[2])
                );
                if (itemMap.containsKey(Integer.valueOf(splited[3]))) {
                    List<Item> przedmioties = itemMap.get(Integer.valueOf(splited[3]));
                    przedmioties.add(item);
                    przedmioties.sort(item);
                    itemMap.replace(Integer.valueOf(splited[3]), przedmioties);
                } else {
                    List<Item> przedmioties = new ArrayList<>();
                    przedmioties.add(item);
                    przedmioties.sort(item);
                    itemMap.put(Integer.valueOf(splited[3]), przedmioties);
                }
            }
            reader.close();
        } catch (Exception e) {
            System.err.format("Exception occurred trying to read '%s'.", "trivial_0.ttp");
            e.printStackTrace();
        }
    }
}
