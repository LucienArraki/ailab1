import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class Generation {
    private List<Indiv> indivList = new ArrayList<>();
    private Random random = new Random();
    private int popSize;

    Generation(int popSize) {
        this.popSize = popSize;
    }

    private Generation(List<Indiv> list, int popSize) {
        indivList = new ArrayList<>(list);
        this.popSize = popSize;
    }

    void initialize(int popSize) {
        this.popSize = popSize;
        for (int i = 0; i < popSize; i++) {
            indivList.add(new Indiv());
        }
    }

    Generation selection(Generation generation, int tour) {
        List<Indiv> newIndivList = new ArrayList<>();
        for (int i = 0; i < popSize; i++) {
            newIndivList.add(selectNextOsobnik(generation, tour));
        }
        return new Generation(newIndivList, popSize);
    }

    private Indiv selectNextOsobnik(Generation generation, int tour) {
        Indiv newIndiv = generation.indivList.get(random.nextInt(popSize));
        for (int i = 0; i < tour; i++) {
            Indiv indiv = generation.indivList.get(random.nextInt(popSize));
            newIndiv = newIndiv.rating < indiv.rating ? indiv : newIndiv;
        }
        return new Indiv(newIndiv);
    }

    Generation crossover(Generation generation, double px) {
        List<Indiv> newIndivList = new ArrayList<>();
        while (newIndivList.size() < popSize) {
            Indiv indiv1 = generation.indivList.get(random.nextInt(popSize));
            Indiv indiv2 = generation.indivList.get(random.nextInt(popSize));
            if (random.nextDouble() < px) {
                List<City> indiv1Genotype = indiv1.getGenotype();
                List<City> indiv2Genotype = indiv2.getGenotype();

                int index = random.nextInt(DataFromFile.cityMap.size() - 2) + 1;
                createChild(newIndivList, indiv2Genotype, indiv1Genotype, index);

                createChild(newIndivList, indiv1Genotype, indiv2Genotype, index);
            } else {
                newIndivList.add(new Indiv(indiv1));
                newIndivList.add(new Indiv(indiv2));
            }
        }
        return new Generation(newIndivList, popSize);
    }

    private void createChild(List<Indiv> newIndivList, List<City> indiv1Citys, List<City> indiv2Citys, int index) {
        List<City> child2Genotype = new ArrayList<>(indiv2Citys.subList(0, index));
        child2Genotype.addAll(indiv1Citys.subList(index, indiv1Citys.size()));
        newIndivList.add(new Indiv(child2Genotype));
    }

    Generation mutation(Generation generation, double pm) {
        List<Indiv> newIndivList = new ArrayList<>();
        for (int i = 0; i < popSize; i++) {
            Indiv indiv = generation.indivList.get(i);
            indiv.mutation(pm);
            newIndivList.add(new Indiv(indiv));
        }
        return new Generation(newIndivList, popSize);
    }

    void evaluate(Results results) {
        double avgRating = 0;
        double avgProfit = 0;
        double avgTime = 0;
        double avgWeight = 0;

        Indiv low = indivList.get(0);
        Indiv best = indivList.get(0);
        for (Indiv x : indivList) {
            best = best.rating < x.rating ? x : best;
            low = low.rating > x.rating ? x : low;
            avgRating += x.rating;
        }
        avgRating = avgRating / indivList.size();
        avgProfit = avgProfit / indivList.size();
        avgTime = avgTime / indivList.size();
        avgWeight = avgWeight / indivList.size();

        results.bestRating.add(best.rating);
        results.avgRating.add(avgRating);
        results.lowRating.add(low.rating);

        results.bestProfit.add(best.profit);
        results.avgProfit.add(avgProfit);
        results.lowProfit.add(low.profit);

        results.bestTime.add(best.timing);
        results.avgTime.add(avgTime);
        results.lowTime.add(low.timing);

        results.bestWeight.add(best.weight);
        results.avgWeight.add(avgWeight);
        results.lowWeight.add(low.weight);
    }
}
