class City {
    int id;
    double x;
    double y;

    City(int id, double x, double y) {
        this.id = id;
        this.x = x;
        this.y = y;
    }


    City(City m) {
        this.id = m.id;
        this.x = m.x;
        this.y = m.y;
    }
}
