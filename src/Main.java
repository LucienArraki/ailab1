import java.io.IOException;

public class Main {
    private static int gen = 1000;

    public static void main(String[] args) {
        Results results = new Results();
        Algorythm algorythm = new Algorythm(100, 0.7, 0.01, 10, results);

        DataFromFile dataFromFile = new DataFromFile();
        dataFromFile.getData();

        long start = System.nanoTime();
        algorythm.initialize();

        for (int i = 0; i < gen; i++) {
            algorythm.nextGeneration(algorythm.generation);
        }
        long stop = System.nanoTime();
        System.out.println("czas trwania: " + (((double) stop - (double) start) / 1000000000));
        System.out.println();

        try {
            algorythm.printResults();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
