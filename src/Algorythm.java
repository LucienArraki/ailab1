import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

class Algorythm {
    Generation generation;
    private Results results;
    private int popSize;
    private double px;
    private double pm;
    private int tour;

    Algorythm(int popSize, double px, double pm, int tour, Results results) {
        this.popSize = popSize;
        this.px = px;
        this.pm = pm;
        this.tour = tour;
        this.generation = new Generation(popSize);
        this.results = results;
    }

    void initialize() {
        generation.initialize(popSize);
    }

    void nextGeneration(Generation generation) {
        this.generation = generation.selection(generation, tour);
        this.generation = generation.crossover(this.generation, px);
        this.generation = generation.mutation(this.generation, pm);
        this.generation.evaluate(results);
    }

    void printResults() throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter("rating.txt"));

        for (int i = 0; i < results.bestRating.size(); i++) {
            writer.write(results.bestRating.get(i) + "\t" + results.avgRating.get(i) + "\t" + results.lowRating.get(i) + "\n");
        }
        writer.close();

        writer = new BufferedWriter(new FileWriter("profit.txt"));

        for (int i = 0; i < results.bestProfit.size(); i++) {
            writer.write(results.bestProfit.get(i) + "\t" + results.avgProfit.get(i) + "\t" + results.lowProfit.get(i) + "\n");
        }
        writer.close();

        writer = new BufferedWriter(new FileWriter("timing.txt"));

        for (int i = 0; i < results.bestTime.size(); i++) {
            writer.write(results.bestTime.get(i) + "\t" + results.avgTime.get(i) + "\t" + results.lowTime.get(i) + "\n");
        }
        writer.close();


        writer = new BufferedWriter(new FileWriter("weight.txt"));

        for (int i = 0; i < results.bestWeight.size(); i++) {
            writer.write(results.bestWeight.get(i) + "\t" + results.avgWeight.get(i) + "\t" + results.lowWeight.get(i) + "\n");
        }
        writer.close();
    }
}
