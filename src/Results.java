import java.util.ArrayList;
import java.util.List;

class Results {
    List<Double> avgRating = new ArrayList<>();
    List<Double> lowRating = new ArrayList<>();
    List<Double> bestRating = new ArrayList<>();

    List<Double> avgProfit = new ArrayList<>();
    List<Double> lowProfit = new ArrayList<>();
    List<Double> bestProfit = new ArrayList<>();

    List<Double> avgTime = new ArrayList<>();
    List<Double> lowTime = new ArrayList<>();
    List<Double> bestTime = new ArrayList<>();

    List<Double> avgWeight = new ArrayList<>();
    List<Double> lowWeight = new ArrayList<>();
    List<Double> bestWeight = new ArrayList<>();
}
